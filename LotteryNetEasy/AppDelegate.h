//
//  AppDelegate.h
//  LotteryNetEasy
//
//  Created by @Zs on 15/7/26.
//  Copyright (c) 2015年 @Zs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

