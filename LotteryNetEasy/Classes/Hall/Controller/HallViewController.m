//
//  HallViewController.m
//  LotteryNetEasy
//
//  Created by @Zs on 15/7/26.
//  Copyright (c) 2015年 @Zs. All rights reserved.
//

#import "HallViewController.h"

@interface HallViewController ()


@property (assign,nonatomic) BOOL isClicked;

@property (weak,nonatomic) UIView *popView;
//懒加载 用strong 控件用strong
@property (strong,nonatomic) UIButton *coverBtn;




@end



@implementation HallViewController



-(void)show{
       self.isClicked = NO;
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view addSubview:self.coverBtn];
        [self.view   addSubview:self.popView];
    }];
    
}

-(void)hidden{
    self.isClicked = YES;
    
    [UIView animateWithDuration:1 animations:^{
        
            [self.coverBtn removeFromSuperview];
        
        [self.popView removeFromSuperview];
    }];
    
}


- (IBAction)activity:(UIBarButtonItem *)sender {
//    
//    UIView *popView = [[UIView alloc]init];
//
//    
//    [popView setFrame:CGRectMake(80, 100, 200, 170)];
//    
//    [self.view addSubview:popView];
//    
//    
//    [popView setBackgroundColor:[UIColor redColor]];
//    
//    popView.center = self.view.center;
    
    
   
    
//    self.popView.hidden = NO;
    
//    [self.view addSubview:self.popView];
    if (self.isClicked) {
     
        [self show];
    }else{

        [self hidden];
    }
    
    
    
    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//            [self createPopView];
//    });
    
    
   
    
}

-(void)createPopView{

    
    UIView *popView = [[NSBundle  mainBundle]loadNibNamed:@"popView" owner:nil options:nil].lastObject;
    
    [popView setFrame:CGRectMake(0, 0, 345, 300)];
    
    
    popView.center = self.view.center;
    
    [self.view addSubview:popView];
    
    
    
//    popView.hidden = YES;
    
//    self.popView = popView;




}

- (void)viewDidLoad {
    
    self.isClicked = YES;
    [super viewDidLoad];
    UIWebView *wb = [[UIWebView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:wb];
    NSString *str = @"http://caipiao.163.com/t";
    NSURL *url = [NSURL URLWithString:str];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [wb loadRequest:request];

    
}

-(UIButton *)coverBtn{

    if (!_coverBtn) {
        _coverBtn = [[UIButton alloc]initWithFrame:self.view.bounds];
        
        
        [_coverBtn setBackgroundColor:[UIColor blackColor]];
        [_coverBtn setAlpha:0.5];
        [_coverBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    }


    
    return _coverBtn;
}


-(void)close{
    
    [self hidden];

}

-(UIView *)popView{


    if (_popView == nil) {
        _popView =[[NSBundle  mainBundle]loadNibNamed:@"popView" owner:nil options:nil].lastObject;
        
        [_popView setFrame:CGRectMake(0, 0, 345, 300)];
        
        
        _popView.center = self.view.center;
        
//        [self.view addSubview:_popView];
        
    }

    return _popView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
