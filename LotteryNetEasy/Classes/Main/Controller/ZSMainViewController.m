//
//  ZSMainViewController.m
//  LotteryNetEasy
//
//  Created by @Zs on 15/7/26.
//  Copyright (c) 2015年 @Zs. All rights reserved.
//

#import "ZSMainViewController.h"
#import "ZSTabBar.h"
@interface ZSMainViewController ()<ZSTabBarDelagate>

@end

@implementation ZSMainViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadChildViewController];

    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    ZSTabBar *barView = [[ZSTabBar alloc]init];
    
    barView.delagate =self;
    
    for (int i = 0; i<self.viewControllers.count; i++) {
        NSString *normal = [NSString stringWithFormat:@"TabBar%d",(i+1)];
        NSString *selected = [NSString stringWithFormat:@"TabBar%dSel",(i+1)];
        
        [barView createBtn:normal selected:selected];
        
        
        
    }
    
    
    [barView setFrame:self.tabBar.frame];
    
    [self.view addSubview:barView];
    
    [self.tabBar removeFromSuperview];
    
    
    
    
    
    // Do any additional setup after loading the view.
}


//加载所有子视图
-(void)loadChildViewController{

    
    
    UINavigationController *hall = [self createView:@"Hall"];
    UINavigationController *arena = [self createView:@"Arena"];
    UINavigationController *discover = [self createView:@"Discover"];
    UINavigationController *history = [self createView:@"History"];
    UINavigationController *myLottery = [self createView:@"MyLottery"];
    
    self.viewControllers  = @[hall,arena,discover,history,myLottery];

}


-(UINavigationController*)createView:(NSString *)name{
    
    
    UIStoryboard *Sb = [UIStoryboard storyboardWithName:name bundle:nil];
    UINavigationController *Nav = [Sb instantiateInitialViewController];


    return Nav;
    
}

-(void)changeTabBar:(ZSTabBar *)btn andIndex:(int)index{
    self.selectedIndex = index;
}


@end
