//
//  ZSBarButton.m
//  LotteryNetEasy
//
//  Created by @Zs on 15/7/26.
//  Copyright (c) 2015年 @Zs. All rights reserved.
//

#import "ZSTabBar.h"
#import "ZSButton.h"
@interface ZSTabBar()

@property (weak,nonatomic) UIButton *selectedBtn;

@end
//自定义一个TabBar
@implementation ZSTabBar

//
////加载视图
//-(instancetype)initWithFrame:(CGRect)frame{
//
//    if (self = [super initWithFrame:frame]) {
//        for (int i = 0; i<5; i++) {
//        
//            
//            NSString *normal = [NSString stringWithFormat:@"TabBar%d",(i+1)];
//            
//            NSString *selected = [NSString stringWithFormat:@"TabBar%dSel",(i+1)];
//            
//            UIButton *button = [[UIButton alloc]init];
//            
//            [button setBackgroundImage:[UIImage imageNamed:normal] forState:UIControlStateNormal];
//            
//            [button setBackgroundImage:[UIImage imageNamed:selected] forState:UIControlStateSelected];
//            
//            
//            [self addSubview:button];
//            
//            if (i==0) {
//                button.selected = YES;
//            }        
// 
//        }
//    }
//    
//    return  self;
//
//}

//加载布局
-(void)layoutSubviews{

    [super layoutSubviews];
    
    
    
    for (int i = 0; i<self.subviews.count; i++) {
        
        CGFloat buttonWidth = self.frame.size.width/self.subviews.count;
        CGFloat buttonHeight = self.frame.size.height;
        CGFloat buttonY = 0;
        CGFloat buttonX = i*buttonWidth;
        UIButton *button = self.subviews[i];
        button.tag = i;
        [button setFrame:CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight)];
        
        if (i ==0 ) {
            button.selected = YES;
            self.selectedBtn = button;
            
        }
    }
    
}

//创建按钮
-(void)createBtn:(NSString *)normal selected:(NSString *)selectedName{
    
    

   
    
    ZSButton *button = [[ZSButton alloc]init];
    
    [button setBackgroundImage:[UIImage imageNamed:normal] forState:UIControlStateNormal];
    
    [button setBackgroundImage:[UIImage imageNamed:selectedName] forState:UIControlStateSelected];
    
    [self addSubview:button];
    
    [button addTarget:self action:@selector(didClickedBtn:) forControlEvents:UIControlEventTouchDown];
    
    
}

//控制器切换
-(void)didClickedBtn:(UIButton *)btn{
    
    if ([self.delagate respondsToSelector:@selector(changeTabBar:andIndex:)]) {
        [self.delagate changeTabBar:self andIndex:(int)btn.tag];
        
    }

    self.selectedBtn.selected = NO;
    
    btn.selected = YES;
    
    self.selectedBtn = btn;
    
}


@end
