//
//  ZSBarButton.h
//  LotteryNetEasy
//
//  Created by @Zs on 15/7/26.
//  Copyright (c) 2015年 @Zs. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZSTabBar;
@protocol ZSTabBarDelagate <NSObject>

-(void)changeTabBar:(ZSTabBar *)btn andIndex:(int) index;


@end




@interface ZSTabBar : UIView
-(void)createBtn:(NSString *)normal selected:(NSString *)selectedName;

@property (weak,nonatomic) id <ZSTabBarDelagate>delagate;


@end
